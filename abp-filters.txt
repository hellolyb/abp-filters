[Adblock Plus 3.1]
! Title: hellolyb
! Expires: 1 hours (update frequency)
! Homepage: https://gitee.com/hellolyb/abp-filters

! 电影先生
##.popup.popup-ts.popup-tc.popupicon.open
##.scroll-box.scroll-domain-prompt
##.app-text

! 歪片星球
##.popup.popup-tips.popup-icon.open
##.shortcuts-mobile-overlay
##.player-recommend-float
##.footer > .content > .links-list

! 欧乐影院
##.hl-nav > .hl-nav-item:nth-child(2)
##.hl-menus > .hl-menus-item:nth-child(2)
##.hl-smnav > .hl-smnav-item:nth-child(8)
https://bestialvehemence.com
##.hl-player-notice

! 哔哩哔哩
bilibili.com##.adblock-tips
! 语雀
yuque.com##.navlink-memberPay